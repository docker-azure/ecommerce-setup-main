. ./env.sh

docker-compose -f docker-compose.yml -f docker-compose.app.yml -p $APP_NAME_DOCKER down

# sudo rm -rf "$APP_VOLUME_DATABASE_MOUNT_PATH"
sudo rm -rf "$APP_VOLUME_CODE_MOUNT_PATH"

sudo mkdir -p "$APP_VOLUME_CODE_MOUNT_PATH"
sudo mkdir -p "$APP_VOLUME_DATABASE_MOUNT_PATH"
sudo mkdir -p "$APP_VOLUME_STORAGE_MOUNT_PATH"
sudo mkdir -p "$APP_VOLUME_LOGS_MOUNT_PATH"
sudo mkdir -p "$APP_VOLUME_UPLOADS_PATH"
sudo mkdir -p "$APP_VOLUME_PUBLIC_PATH"

docker-compose -f docker-compose.yml -f docker-compose.app.yml -p $APP_NAME_DOCKER up --build -d
