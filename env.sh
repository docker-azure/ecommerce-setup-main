export APP_NAME_DOCKER='ecommerce'
export DOCKER_NETWORK="nginx-proxy"
 
export REGISTRY_NGINX_BACKEND='registry.gitlab.com/docker-azure/nginx-backend-main:latest'
export REGISTRY_BACKEND='registry.gitlab.com/docker-azure/backend-main:latest'
export REGISTRY_USER_FRONTEND='registry.gitlab.com/docker-azure/user-main:latest'
export REGISTRY_ADMIN_FRONTEND='registry.gitlab.com/docker-azure/admin-main:latest'

export APP_VOLUME_CODE_MOUNT_PATH="/home/tuantien2k1/data/web-data/${APP_NAME_DOCKER}/code"
export APP_VOLUME_DATABASE_MOUNT_PATH="/home/tuantien2k1/data/web-data/${APP_NAME_DOCKER}/database"
export APP_VOLUME_STORAGE_MOUNT_PATH="/home/tuantien2k1/data/web-data/${APP_NAME_DOCKER}/storage"
export APP_VOLUME_LOGS_MOUNT_PATH="/home/tuantien2k1/data/web-data/${APP_NAME_DOCKER}/logs"
export APP_VOLUME_UPLOADS_PATH="/home/tuantien2k1/data/web-data/${APP_NAME_DOCKER}/uploads"
export APP_VOLUME_PUBLIC_PATH="/home/tuantien2k1/data/web-data/${APP_NAME_DOCKER}/public"

export ADMIN_API_HOST='api.hstore.pw'
export PMA_DB_HOST='pma.hstore.pw'
export USER_FRONTEND_HOST='hstore.pw,www.hstore.pw'
export ADMIN_FRONTEND_HOST='admin.hstore.pw'
export VIRTUAL_NETWORK='nginx-proxy'
export VIRTUAL_PORT='80'

export LETSENCRYPT_ADMIN_API_HOST="$ADMIN_API_HOST"
export LETSENCRYPT_USER_FRONTEND_HOST="$USER_FRONTEND_HOST"
export LETSENCRYPT_ADMIN_FRONTEND_HOST="$ADMIN_FRONTEND_HOST"
export LETSENCRYPT_PMA_DB_HOST="$PMA_DB_HOST"
export LETSENCRYPT_EMAIL='crtrunghau@gmail.com'

export APP_NAME='MW Store '
export APP_ENV='dev'
export APP_KEY='base64:zwW1RI0AezN7IQU8xRJJQbIJ4vVsCZVf4Ibu+1jDUK0='
export APP_DEBUG='true'
export APP_URL="https://${ADMIN_API_HOST}"
export REDIRECT_HTTPS='true'

export LOG_CHANNEL='stack'

export DB_CONNECTION='mysql'
export DB_HOST='mysql'
export DB_PORT='3306'
export DB_DATABASE='mwstore'
export DB_USERNAME='crtrunghau@gmail.com'
export DB_PASSWORD='trunghau11332244'

export BROADCAST_DRIVER='log'
export CACHE_DRIVER='file'
export QUEUE_CONNECTION='database'
export SESSION_DRIVER='file'
export SESSION_LIFETIME='120'

export REDIS_HOST='127.0.0.1'
export REDIS_PASSWORD='null'
export REDIS_PORT='6379'

export MAIL_DRIVER='smtp'
export MAIL_HOST='mail.trunghau.website'
export MAIL_PORT='587'
export MAIL_USERNAME='trunghau@trunghau.pw'
export MAIL_PASSWORD='trunghau11332244'
export MAIL_ENCRYPTION=''
# export MAIL_FROM='xxx'
# export MAIL_TO='xxx'
export MAIL_FROM_ADDRESS='trunghau@trunghau.pw'
export MAIL_FROM_NAME='MW Store'

export GOOGLE_CLIENT_ID=46850447107-5f4t1il9cui0lcmfph87dnfd1nm6f0f1.apps.googleusercontent.com
export GOOGLE_CLIENT_SECRET=rU0auv3H17v_KoXzS-KvjHxd
export GOOGLE_REDIRECT_URI=http://localhost:8000/signin/google-social
export GOOGLE_REDIRECT_URI_CLIENT=https://hstore.pw/signin/callback/google

export FACEBOOK_CLIENT_ID=1044260896507355
export FACEBOOK_CLIENT_SECRET=8c088be5126894a9b02a442bc282edda
export FACEBOOK_REDIRECT_URI=http://localhost:8000/signin/facebook-social
export FACEBOOK_REDIRECT_URI_CLIENT=https://hstore.pw/signin/callback/facebook

export ZALO_CLIENT_ID=712756656876844245
export ZALO_CLIENT_SECRET=mIgF4MWkR3I9zKGXc2HQ
export ZALO_REDIRECT_URI=https://trunghau.website/signin/zalo-social

export GITHUB_CLIENT_ID=620bbca2f647822227bf
export GITHUB_CLIENT_SECRET=b1abe05c90f0a144082e84ea5cf403d8569a05ad
export GITHUB_REDIRECT_URI=https://hstore.pw/signin/callback/github

export LINKEDIN_CLIENT_ID=86nkv62fqr3lxw
export LINKEDIN_CLIENT_SECRET=XxHu4tAsLrzyk7rz
export LINKEDIN_REDIRECT_URI=https://hstore.pw/signin/callback/linkedin

export NOCAPTCHA_SITEKEY=6LfV4XAbAAAAAGsfbSmN2PNpGej4vgLwx7nsIBxf
export NOCAPTCHA_SECRET=6LfV4XAbAAAAAMkOwmHItpvKrhbw0COYZU5NjB69

export VPN_TEST_MODE=true
export VPN_TMN_CODE=E9D6HP00
export VPN_HASH_SERECT=QMNMCECRILLJTQWOCVWHFFWGHFYMROHN
export VPN_URL=http://sandbox.vnpayment.vn/paymentv2/vpcpay.html

export MOMO_TEST_MODE=true
export MOMO_ACCESS_KEY=imKX2GZ0f5sBdC2P
export MOMO_SECRET_KEY=tPg77LWSDmfBgVlbESNqA7BVEuF5w80G
export MOMO_PARTNER_CODE=MOMO2NZE20210711

#sandbox or live
export PAYPAL_MODE=sandbox
#Paypal sandbox credential
export PAYPAL_SANDBOX_CLIENT_ID=ASXfoCEQRQ-qKZRRpEZj_dNdgjbOOr7afjKIovaTvmDW4i2K3an-Wv5MgcMGfmhRgaeHg7xHu2orRzR5
export PAYPAL_SANDBOX_CLIENT_SECRET=EPQD_XYiOa-18of4TSaDOH4_-IH3yBGycVOP5um39vTD-tZ3NOpy8p0DFAHD5bXjpu-TtPdUEG_bYXll
#Paypal live credential
export PAYPAL_LIVE_CLIENT_ID=Jjac8ad......f2sd2faww
export PAYPAL_LIVE_CLIENT_SECRET=cak8nd0Pq3mQf2f......jawd2z2d6

export RETURN_URL_PAYMENT=http://localhost:8000/order/payment_callback
export RETURN_URL_PAYMENT_CLIENT=https://hstore.pw/order/callback

export FIREBASE_SERVER_KEY=AAAAdkisvGQ:APA91bGNq_LMV43tF4tl2Ylpx7WsWWtvvTFDQkLxhhQf0LNz1jXZaabzR0CjkwUIpzFJMdO0WS-HE8W7O7QTSJ0f8-zaIb96VEfEVQD7KFRd1a00LCypkWZdvkUiyL37Xfr8HVZElZWo

export FE_URL='user.localhost'
export GOOGLE_RECAPTCHA_SECRET=""
